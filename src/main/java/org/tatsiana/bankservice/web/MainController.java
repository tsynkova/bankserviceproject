package org.tatsiana.bankservice.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.tatsiana.bankservice.model.ProviderBankInfo;
import org.tatsiana.bankservice.service.BankService;

@RestController
public class MainController {

    @Autowired
    private BankService bankService;

    @RequestMapping(value = "/agents/{agentId}/providerBankInfo/search", method = RequestMethod.POST, headers = "Accept=application/json;charset=UTF-8")
    public
    @ResponseBody
    ProviderBankInfo getAllProviderBankInfo(@PathVariable String agentId, @RequestParam(required = false, defaultValue = "") String bankId) {
        ProviderBankInfo providerBankInfo = bankService.getByAgentIdAndBankId(agentId, bankId);
        return providerBankInfo;
    }

    @RequestMapping(value = "/agents/{agentId}/providerBankInfo", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
    public
    @ResponseBody
    ProviderBankInfo getProviderBankInfo(@PathVariable String agentId, @RequestParam(required = false, defaultValue = "") String bankId) {
        ProviderBankInfo providerBankInfo = bankService.getByAgentIdAndBankId(agentId, bankId);
        return providerBankInfo;
    }
}
