package org.tatsiana.bankservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class OperatorInfo implements Serializable {

    @JsonIgnore
    private long id;
    private String recipientAccount;
    private String INN;
    private String KPP;

    public OperatorInfo() {
    }

    public OperatorInfo(long id, String recipientAccount, String INN, String KPP) {
        this.id = id;
        this.recipientAccount = recipientAccount;
        this.INN = INN;
        this.KPP = KPP;
    }

    @JsonIgnore
    public long getId() {
        return id;
    }

    @JsonProperty
    public void setId(long id) {
        this.id = id;
    }

    public String getRecipientAccount() {
        return recipientAccount;
    }

    public void setRecipientAccount(String recipientAccount) {
        this.recipientAccount = recipientAccount;
    }

    public String getINN() {
        return INN;
    }

    public void setINN(String INN) {
        this.INN = INN;
    }

    public String getKPP() {
        return KPP;
    }

    public void setKPP(String KPP) {
        this.KPP = KPP;
    }

    @Override
    public String toString() {
        return "operatorInfo{" +
                ", recipientAccount='" + recipientAccount + '\'' +
                ", INN='" + INN + '\'' +
                ", KPP='" + KPP + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperatorInfo that = (OperatorInfo) o;
        return id == that.id &&
                Objects.equals(recipientAccount, that.recipientAccount) &&
                Objects.equals(INN, that.INN) &&
                Objects.equals(KPP, that.KPP);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, recipientAccount, INN, KPP);
    }
}
