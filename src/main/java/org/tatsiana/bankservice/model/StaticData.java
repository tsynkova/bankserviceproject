package org.tatsiana.bankservice.model;

import java.util.ArrayList;
import java.util.List;

public class StaticData {

    public static List<ProviderBankInfo> providerBankInfoList = new ArrayList<ProviderBankInfo>();

    public static List<ProviderBankInfo>  getProviderBankInfoList() {
        OperatorInfo providerInfo1 = new OperatorInfo(1, "aa", "aa", "aa");
        BankInfo bankBaseInfo1 = new BankInfo(1, "a", "a", "a","a","a", true);
        providerBankInfoList.add(new ProviderBankInfo(1, providerInfo1, bankBaseInfo1));

        OperatorInfo providerInfo2 = new OperatorInfo(2, "bb", "bb", "bb");
        BankInfo bankBaseInfo2 = new BankInfo(2, "b", "b", "b","b","b", true);
        providerBankInfoList.add(new ProviderBankInfo(2, providerInfo2, bankBaseInfo2));

        OperatorInfo providerInfo3 = new OperatorInfo(3, "vv", "vv", "vv");
        BankInfo bankBaseInfo3 = new BankInfo(3, "v", "v", "v","v","v", true);
        providerBankInfoList.add(new ProviderBankInfo(3, providerInfo3, bankBaseInfo3));

        OperatorInfo providerInfo4 = new OperatorInfo(4, "hh", "hh", "hh");
        BankInfo bankBaseInfo4 = new BankInfo(4, "h", "h", "h","h","h", true);
        providerBankInfoList.add(new ProviderBankInfo(4, providerInfo4, bankBaseInfo4));

        OperatorInfo providerInfo5 = new OperatorInfo(5, "pp", "pp", "pp");
        BankInfo bankBaseInfo5 = new BankInfo(5, "p", "p", "p","p","p", true);
        providerBankInfoList.add(new ProviderBankInfo(5, providerInfo5, bankBaseInfo5));

        return providerBankInfoList;
    }

}
